package learn.algo.questions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JewelsAndStonesTest {

    @Test
    void testNumJewelsInStones() {
        JewelsAndStones js = new JewelsAndStones();
        assertEquals(3, js.numJewelsInStones("aA", "aAAbbbb"));
        assertEquals(0, js.numJewelsInStones("z", "ZZ"));
    }
}
