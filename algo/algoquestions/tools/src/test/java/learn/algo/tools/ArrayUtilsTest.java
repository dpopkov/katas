package learn.algo.tools;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayUtilsTest {

    private final static int[] nonDecreasing = {1, 3, 3, 5};
    private final static int[] nonIncreasing = {5, 3, 3, 1};

    @Test
    void testIsNonDecreasing() {
        assertTrue(ArrayUtils.isNonDecreasing(nonDecreasing));
        assertFalse(ArrayUtils.isNonDecreasing(nonIncreasing));
    }

    @Test
    void testIsNonIncreasing() {
        assertTrue(ArrayUtils.isNonIncreasing(nonIncreasing));
        assertFalse(ArrayUtils.isNonIncreasing(nonDecreasing));
    }
}
