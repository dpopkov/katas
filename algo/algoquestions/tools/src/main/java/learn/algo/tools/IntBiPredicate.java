package learn.algo.tools;

@FunctionalInterface
public interface IntBiPredicate {
    boolean test(int t, int u);
}
