package learn.algo.tools;

public class ArrayUtils {

    public static boolean isNonDecreasing(int[] array) {
        if (isTooSmall(array)) return true;
        return traverseWittFalsePredicate(array, (prev, current) -> prev > current);
    }

    public static boolean isNonIncreasing(int[] array) {
        if (isTooSmall(array)) return true;
        return traverseWittFalsePredicate(array, (prev, current) -> prev < current);
    }

    private static boolean isTooSmall(int[] array) {
        return array.length < 2;
    }

    private static boolean traverseWittFalsePredicate(int[] array, IntBiPredicate predicate) {
        int prev = array[0];
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            if (predicate.test(prev, current)) {
                return false;
            }
            prev = current;
        }
        return true;
    }
}
