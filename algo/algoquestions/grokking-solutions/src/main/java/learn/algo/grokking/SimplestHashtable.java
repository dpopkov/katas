package learn.algo.grokking;

public class SimplestHashtable<E> {

    private static final int DEFAULT_TABLE_SIZE = 16;

    private final Node[] table;

    public SimplestHashtable() {
        this(DEFAULT_TABLE_SIZE);
    }

    SimplestHashtable(int capacity) {
        table = new Node[capacity];
    }

    public void put(String key, E value) {
        int tableIdx = calculateBucketIndex(key);
        final Node newNode = new Node(key, value);
        if (table[tableIdx] == null) {
            table[tableIdx] = newNode;
        } else {
            Node node = table[tableIdx];
            if (node.key.equals(key)) {
                node.value = value;
                return;
            }
            while (node.next != null) {
                node = node.next;
                if (node.key.equals(key)) {
                    node.value = value;
                    return;
                }
            }
            node.next = newNode;
        }
    }

    @SuppressWarnings("unchecked")
    public E get(String key) {
        Node node = table[calculateBucketIndex(key)];
        if (node != null) {
            while (node != null && !node.key.equals(key)) {
                node = node.next;
            }
            if (node != null) {
                return (E) node.value;
            }
        }
        return null;
    }

    private static class Node {
        final String key;
        Object value;
        Node next;

        public Node(String key, Object value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "key='" + key + '\'' +
                    ", value=" + value +
                    '}';
        }
    }

    private int calculateBucketIndex(String key) {
        return key.hashCode() % table.length;
    }
}
