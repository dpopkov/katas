package learn.algo.grokking;

public class MergeSort {
    public static void sort(int[] array) {
        sortRecursively(array, 0, array.length - 1);
    }

    private static void sortRecursively(int[] array, int from, int to) {
        if (from >= to) {
            return;
        }
        int middle = from + (to - from) / 2;
        sortRecursively(array, from, middle);
        sortRecursively(array, middle + 1, to);
        merge(array, from, middle, to);
    }

    private static void merge(int[] array, int from, int middle, int to) {
        int[] temp = new int[to - from + 1];
        int j = from;
        int k = middle + 1;
        int i = 0;
        while (j <= middle && k <= to) {
            if (array[j] < array[k]) {
                temp[i] = array[j];
                j++;
            } else {
                temp[i] = array[k];
                k++;
            }
            i++;
        }
        while (j <= middle) {
            temp[i] = array[j];
            j++;
            i++;
        }
        while (k <= to) {
            temp[i] = array[k];
            k++;
            i++;
        }
        System.arraycopy(temp, 0, array, from, temp.length);
    }
}
