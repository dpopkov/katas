package learn.algo.grokking;

public class InsertSort {

    public static void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int v = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] < v) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = v;
        }
    }
}
