package learn.algo.grokking;

public class SelectionSort {
    public static void sort(int[] array) {
        final int last = array.length - 1;
        for (int i = 0; i < last; i++) {
            int jMax = findMax(array, i);
            if (i != jMax) {
                int tmp = array[i];
                array[i] = array[jMax];
                array[jMax] = tmp;
            }
        }
    }

    private static int findMax(int[] array, int from) {
        int max = array[from];
        int iMax = from;
        for (int i = from + 1; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
                iMax = i;
            }
        }
        return iMax;
    }
}
