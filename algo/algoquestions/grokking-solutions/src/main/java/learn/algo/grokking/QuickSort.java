package learn.algo.grokking;

public class QuickSort {
    public static void sort(int[] array) {
        innerSort(array, 0, array.length - 1);
    }

    private static void innerSort(int[] array, int begin, int end) {
        if (begin < end) {
            int partIdx = partition(array, begin, end);
            innerSort(array, begin, partIdx - 1);
            innerSort(array, partIdx + 1, end);
        }
    }

    private static int partition(int[] array, int begin, int end) {
        int pivot = array[end];
        int i = begin - 1;
        for (int j = begin; j < end; j++) {
            if (array[j] <= pivot) {
                i++;
                swap(array, i, j);
            }
        }
        final int partitionIdx = i + 1;
        swap(array, partitionIdx, end);
        return partitionIdx;
    }

    private static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
}
