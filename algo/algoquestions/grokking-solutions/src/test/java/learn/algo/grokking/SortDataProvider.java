package learn.algo.grokking;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

/**
 * Contains arguments for tests which check sorting algorithms.
 */
class SortDataProvider implements ArgumentsProvider {
    @SuppressWarnings("RedundantCast")
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of((Object) new int[] {11, 15, 5, 17, 3, 5}),
                Arguments.of((Object) new int[] {1, 3, 5, 7}),
                Arguments.of((Object) new int[] {7, 5, 3, 1}),
                Arguments.of((Object) new int[] {1})
        );
    }
}
