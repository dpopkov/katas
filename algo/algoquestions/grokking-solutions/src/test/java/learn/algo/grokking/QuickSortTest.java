package learn.algo.grokking;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.Arrays;

import static learn.algo.tools.ArrayUtils.isNonDecreasing;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QuickSortTest {

    @ParameterizedTest
    @ArgumentsSource(SortDataProvider.class)
    void testSort(int[] array) {
        QuickSort.sort(array);
        boolean sorted = isNonDecreasing(array);
        assertTrue(sorted, () -> "Array " + Arrays.toString(array) + " must be non-decreasing");
    }
}
