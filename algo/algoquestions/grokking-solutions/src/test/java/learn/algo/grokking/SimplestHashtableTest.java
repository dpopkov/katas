package learn.algo.grokking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimplestHashtableTest {

    private static final String KEY_1 = "key1";
    private static final String KEY_2 = "key2";
    private static final String KEY_3 = "key3";
    private static final String VALUE_1 = "value1";
    private static final String VALUE_2 = "value2";
    private static final String VALUE_3 = "value3";

    private SimplestHashtable<String> hashtable;

    @Test
    void canPutValueAndGetItBack() {
        hashtable = new SimplestHashtable<>();
        String value = hashtable.get(KEY_1);
        assertNull(value);
        hashtable.put(KEY_1, VALUE_1);
        assertGet(KEY_1, VALUE_1);
        hashtable.put(KEY_2, VALUE_2);
        assertGet(KEY_2, VALUE_2);
    }

    @Test
    void canPutValuesInTheSameBucketAndGetItBack() {
        hashtable = new SimplestHashtable<>(1);
        String value = hashtable.get(KEY_1);
        assertNull(value);
        value = hashtable.get(KEY_2);
        assertNull(value);
        value = hashtable.get(KEY_3);
        assertNull(value);
        hashtable.put(KEY_1, VALUE_1);
        hashtable.put(KEY_2, VALUE_2);
        hashtable.put(KEY_3, VALUE_3);
        assertGet(KEY_1, VALUE_1);
        assertGet(KEY_2, VALUE_2);
        assertGet(KEY_3, VALUE_3);
    }

    @Test
    void canPutAndUpdateValues() {
        hashtable = new SimplestHashtable<>();
        String value = hashtable.get(KEY_1);
        assertNull(value);
        hashtable.put(KEY_1, VALUE_1);
        assertGet(KEY_1, VALUE_1);
        hashtable.put(KEY_1, VALUE_2);
        assertGet(KEY_1, VALUE_2);
    }

    private void assertGet(String key, String expectedValue) {
        String foundValue = hashtable.get(key);
        assertNotNull(foundValue);
        assertEquals(expectedValue, foundValue);
    }
}
