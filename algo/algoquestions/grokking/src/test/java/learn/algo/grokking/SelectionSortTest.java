package learn.algo.grokking;

import learn.algo.tools.ArrayUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.*;

class SelectionSortTest {

    @ParameterizedTest
    @ArgumentsSource(SortDataProvider.class)
    void testSort(int[] array) {
        SelectionSort.sort(array);
        boolean sorted = ArrayUtils.isNonIncreasing(array);
        assertTrue(sorted);
    }
}
