Algo Questions
==============

|   | Question  | Kata | Solution |
|---|-----------|------|----------|
| E | Reverse Single Linked list.    | [ReverseSingleLinked](questions/src/main/java/learn/algo/questions/ReverseSingleLinkedList.java)  | [ReverseSingleLinked](questions-solutions/src/main/java/learn/algo/questions/ReverseSingleLinkedList.java)  |
| E | Find indices of the two numbers such that they add up to target.    | [TwoSum](questions/src/main/java/learn/algo/questions/TwoSum.java)  | [TwoSum](questions-solutions/src/main/java/learn/algo/questions/TwoSum.java)  |
| E | Count how many of the stones you have are also jewels.    | [JewelsAndStones](questions/src/main/java/learn/algo/questions/JewelsAndStones.java)  | [JewelsAndStones](questions-solutions/src/main/java/learn/algo/questions/JewelsAndStones.java)  |


Grokking
========

|   | Title     | Kata | Test | Solution |
|---|-----------|------|------|----------|
| E | Binary Search | [BinarySearch](grokking/src/main/java/learn/algo/grokking/BinarySearch.java) | [BinarySearchTest](grokking/src/test/java/learn/algo/grokking/BinarySearchTest.java) | [BinarySearch](grokking-solutions/src/main/java/learn/algo/grokking/BinarySearch.java)  |
| E | Selection Sort | [SelectionSort](grokking/src/main/java/learn/algo/grokking/SelectionSort.java) | [SelectionSortTest](grokking/src/test/java/learn/algo/grokking/SelectionSortTest.java) | [SelectionSort](grokking-solutions/src/main/java/learn/algo/grokking/SelectionSort.java)  |
| E | Insertion Sort | [InsertSort](grokking/src/main/java/learn/algo/grokking/InsertSort.java) | [InsertSortTest](grokking/src/test/java/learn/algo/grokking/InsertSortTest.java) | [InsertSort](grokking-solutions/src/main/java/learn/algo/grokking/InsertSort.java)  |
| E | Merge Sort | [MergeSort](grokking/src/main/java/learn/algo/grokking/MergeSort.java) | [MergeSortTest](grokking/src/test/java/learn/algo/grokking/MergeSortTest.java) | [InsertSort](grokking-solutions/src/main/java/learn/algo/grokking/MergeSort.java)  |
| E | Quick Sort | [QuickSort](grokking/src/main/java/learn/algo/grokking/QuickSort.java) | [QuickSortTest](grokking/src/test/java/learn/algo/grokking/QuickSortTest.java) | [QuickSort](grokking-solutions/src/main/java/learn/algo/grokking/QuickSort.java)  |
| E | Simplest Hashtable | [SimplestHashtable](grokking/src/main/java/learn/algo/grokking/SimplestHashtable.java) | [SimplestHashtableTest](grokking/src/test/java/learn/algo/grokking/SimplestHashtableTest.java) | [SimplestHashtable](grokking-solutions/src/main/java/learn/algo/grokking/SimplestHashtable.java)  |
