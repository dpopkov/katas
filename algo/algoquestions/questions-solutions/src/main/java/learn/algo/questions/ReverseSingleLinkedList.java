package learn.algo.questions;

public class ReverseSingleLinkedList {

    static Node reverse(Node node) {
        Node current = node;
        Node previous = null;
        while (current != null) {
            Node nextNode = current.next;
            current.next = previous;
            previous = current;
            current = nextNode;
        }
        return previous;
    }

    static class Node {
        final int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(value);
            Node n = next;
            while (n != null) {
                sb.append(",").append(n.value);
                n = n.next;
            }
            sb.append("]");
            return sb.toString();
        }

    }

    static Node of(int... values) {
        Node n = new Node(values[0]);
        Node last = n;
        for (int i = 1; i < values.length; i++) {
            Node other = new Node(values[i]);
            last.next = other;
            last = other;
        }
        return n;
    }
}
