package learn.algo.questions;

/*
You're given strings jewels representing the types of stones that are jewels,
and stones representing the stones you have.
Each character in stones is a type of stone you have. You want to know how many of the stones you have are also jewels.

Letters are case sensitive, so "a" is considered a different type of stone from "A".

Example 1:
Input: jewels = "aA", stones = "aAAbbbb"
Output: 3

Example 2:
Input: jewels = "z", stones = "ZZ"
Output: 0
*/
public class JewelsAndStones {
    public int numJewelsInStones(String jewels, String stones) {
        LatinCharSet set = new LatinCharSet(jewels);
        return (int) stones.chars().filter(set::contains).count();
    }

    private static class LatinCharSet {
        private static final char FROM_CHAR = 'A';
        private static final char TO_CHAR = 'z';

        private final boolean[] set = new boolean[TO_CHAR - FROM_CHAR + 1];

        public LatinCharSet(String letters) {
            letters.chars().forEach(ch -> set[ch - FROM_CHAR] = true);
        }

        public boolean contains(int character) {
            return set[character - FROM_CHAR];
        }
    }
}
