package learn.algo.questions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseSingleLinkedListTest {

    @Test
    void reverse() {
        ReverseSingleLinkedList.Node n = ReverseSingleLinkedList.of(10, 20, 30, 40);
        ReverseSingleLinkedList.Node actual = ReverseSingleLinkedList.reverse(n);
        String expected = "[40,30,20,10]";
        assertNotNull(actual);
        assertEquals(expected, actual.toString());
    }
}
